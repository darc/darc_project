# DARC - Data Anonymisation and Re-identification Competition

This is a python application of DARC. DARC is a competition held at PETs on the
AIcrowd plateform. To participate please go on AIcrowd
[website](https://www.aicrowd.com/challenges/data-anonymization-and-re-identification-competition-darc).

## Getting Started

These instructions will get you a copy of the project up and running on your
local machine for development and testing purposes. See deployment for notes on
how to deploy the project on a live system.

### Installing

Install the requirements via pipenv:

```bash
pipenv install
```

## Running the tests

To run the test on the core execute the following command:

```bash
pipenv run python main.py
```

## Usage of the DARC core

To use the DARC core just import it in your project as :

```python
import darc_core
from darc_core.preprocessing import round1_preprocessing
from darc_core.utils import check_format_trans_file

# Read database from files
ground_truth, submission = round1_preprocessing(
    self.answer_file_path, submission_file_path
)

# Check the format of the Anonymized Transaction file
check_format_trans_file(ground_truth, submission)

# Run metrics for a submission (AT)
metric = darc_core.Metrics(ground_truth, submission)
scores = metric.scores()
```

## Contributing

Please free to contributing in the darc\_core code. Espetially in term of
optimization or architecture model.

## Authors

All the persons below have participated to DARC implementation and deployements

* **Antoine Laurent** - [Ph.D candidate at UQAM](mailto:laurent.antoine@courrier.uqam.ca)
* **Sébastien Gambs** - [Professor at UQAM](mailto:gambs.sebastien@uqam.ca)
* **Louis Béziaud** - [Ph.D candidate at UQAM](mailto:laurent.antoine@courrier.uqam.ca)
* **Sharada Mohanty** - [CEO & Co-funder of AIcrowd](mailto:sharada.mohanty@epfl.ch)
* **Yoann Pagnoux** - [Master Student at INSA](mailto:yoann.pagnoux@insa-cvl.fr)

And of course all the people from the comite.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* We would like to thank all our colleagues, previous trainees and students who
  gave their time and energy for the competition test.
