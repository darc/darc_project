from darc_core.metrics import Metrics
from darc_core.preprocessing import (
    read_tar,
    round1_preprocessing,
    round2_preprocessing,
)
from darc_core.utils import check_format_f_file, check_format_trans_file


def round1(
    answer_file_path,
    submission_file_path,
):
    ground_truth, submission = round1_preprocessing(
        answer_file_path,
        submission_file_path,
    )

    check_format_trans_file(ground_truth, submission)
    metric = Metrics(ground_truth, submission)
    scores = metric.scores()

    _result_object = {
        "score": (max(scores[0:6]) + max(scores[6:13])) / 2,
        "score_secondary": max(scores[0:6]),
        "meta": {
            "e1": scores[0],
            "e2": scores[1],
            "e3": scores[2],
            "e4": scores[3],
            "e5": scores[4],
            "e6": scores[5],
            "s1": scores[6],
            "s2": scores[7],
            "s3": scores[8],
            "s4": scores[9],
            "s5": scores[10],
            "s6": scores[11],
            "s7": scores[12],
        },
    }

    return _result_object


def round2(
    answer_file_path,
    anonimized_submission_file_path,
    submission_file_path,
):
    ground_truth, at_origin = round1_preprocessing(
        answer_file_path,
        anonimized_submission_file_path,
    )

    submission_file_path, _ = read_tar(submission_file_path)

    submission = round2_preprocessing(submission_file_path)

    check_format_f_file(submission)

    metrics = Metrics(ground_truth, at_origin)
    reidentification_score = metrics.compare_f_files(submission)

    _result_object = {
        "score": reidentification_score,
    }

    return _result_object


if __name__ == "__main__":
    answer_file_path = "data/ground_truth.csv"
    r1_submission_file_path = "data/example_files/submission_DEL.csv"
    r2_submission_file_path = "data/example_files/F_a_attempt_2.tar"

    res1 = round1(answer_file_path, r1_submission_file_path)
    print(res1)

    res2 = round2(
        answer_file_path,
        r1_submission_file_path,
        r2_submission_file_path,
    )
    print(res2)
